package no.ntnu.idatg2003.oblig3.cardgame;

/**
 * Represents a deck of playing cards. A deck of playing cards has 52 cards, with
 * 13 cards in each of 4 suits: spades, hearts, diamonds and clubs. Each suit
 * has one card of each face value: Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack,
 * Queen and King.
 * <p>
 *   The deck of cards can be shuffled, and cards can be dealt from the deck.
 *   The deck is initially ordered, with the Ace of Spades at the bottom and the
 *   King of Clubs at the top.
 *   </p>
 *
 * @author Kim Hejer
 * @version 0.1.0
 * @since 0.1.0
 * @see PlayingCard
 */
public class DeckOfCards {

  public static final int DECK_SIZE = 52;
  private PlayingCard[] deck;
  private int topCardIndex;

  /**
   * Creates a new deck of cards, with 52 cards in a standard deck.
   */
  public DeckOfCards() {
    deck = new PlayingCard[DECK_SIZE];
    topCardIndex = 0;
    int i = 0;
    for (char suit : new char[] {'S', 'H', 'D', 'C'}) {
      for (int face = 1; face <= 13; face++) {
        deck[i++] = new PlayingCard(suit, face);
      }
    }
  }

  /**
   * Shuffles the deck of cards.
   */
  public void shuffle() {
    for (int i = 0; i < DECK_SIZE; i++) {
      int j = (int) (Math.random() * DECK_SIZE);
      PlayingCard temp = deck[i];
      deck[i] = deck[j];
      deck[j] = temp;
    }
  }

  /**
   * Deals one card from the deck. If the deck is empty, null is returned.
   *
   * @return the top card from the deck, or null if the deck is empty
   */
  public PlayingCard dealOneCard() {
    if (topCardIndex < DECK_SIZE) {
      return deck[topCardIndex++];
    } else {
      shuffle();
      topCardIndex = 0;
      return deck[topCardIndex++];
    }
  }

  /**
   * Deals n cards from the deck. If the deck is empty, an empty array is returned.
   *
   * @param n the number of cards to deal
   * @return an array of n cards, or an empty array if the deck is empty
   * @throws IllegalArgumentException if n is less than 1 or greater than the number of cards left in the deck
   */
  public PlayingCard[] dealCards(int n) {
    if (n <= 0) {
      throw new IllegalArgumentException("Parameter n must be a positive number above 0");
    }
    if (n > DECK_SIZE) {
      throw new IllegalArgumentException("Not enough cards in the deck");
    }
    PlayingCard[] cards = new PlayingCard[n];
    for (int i = 0; i < n; i++) {
      if (topCardIndex >= DECK_SIZE) {
        shuffle();
        topCardIndex = 0;
      }
      cards[i] = deck[topCardIndex++];
    }
    return cards;
  }

  public int getTopCardIndex() {
    return topCardIndex;
  }

  public void setTopCardIndex(int topCardIndex) {
    this.topCardIndex = topCardIndex;
  }

  public PlayingCard[] getDeck() {
    return deck;
  }

  public void setDeck(PlayingCard[] deck) {
    this.deck = deck;
  }
}
