package no.ntnu.idatg2003.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FXGUI extends Application {

  DeckOfCards deck = new DeckOfCards();
  PlayingCard[] hand;
  int sumOfFaces;

  @Override
  public void start(Stage primaryStage) {
    deck.shuffle();
    // Create display window
    TextArea displayWindow = new TextArea();
    displayWindow.setEditable(false);
    displayWindow.setPrefRowCount(5);

    // Create buttons
    Button dealHandButton = new Button("Deal Hand");
    Button checkHandButton = new Button("Check Hand");

    // Create text fields for displaying sum of faces
    TextField sumOfFacesTextField = new TextField();
    sumOfFacesTextField.setEditable(false);
    sumOfFacesTextField.setPromptText("Sum of Faces");
    TextField cardsOfHeartsTextField = new TextField();
    cardsOfHeartsTextField.setEditable(false);
    cardsOfHeartsTextField.setPromptText("Cards of Hearts");
    TextField flushTextField = new TextField();
    flushTextField.setEditable(false);
    flushTextField.setPromptText("Yes or No");
    TextField queenOfSpadesTextField = new TextField();
    queenOfSpadesTextField.setEditable(false);
    queenOfSpadesTextField.setPromptText("Yes or No");

    // Set action event handlers for the buttons
    dealHandButton.setOnAction(e -> {
      // Define action when "DealHand" button is clicked
      displayWindow.clear();
      hand = deck.dealCards(5);
      for (PlayingCard card : hand) {
        displayWindow.appendText(card.getAsString() + "\n");
      }
    });

    checkHandButton.setOnAction(e -> {
      // Define action when "CheckHand" button is clicked
      sumOfFaces = 0;
      String cardsOfHearts = "";
      int index = 0;
      int flushIndex = 0;
      queenOfSpadesTextField.setText("No");

      for (PlayingCard card : hand) {
        if (card.getFace() == 12 && card.getSuit() == 'S') {
          queenOfSpadesTextField.setText("Yes");
        }
        if (card.getSuit() == 'H') {
          cardsOfHearts += card.getAsString() + " ";
        }
        sumOfFaces += card.getFace();
        index++;
        if (index < 5) {
          if (hand[index].getSuit() == card.getSuit()) {
            flushIndex++;
          }
        }
      }
      if (flushIndex == 4) {
        flushTextField.setText("Yes");
      } else {
        flushTextField.setText("No");
      }
      // check if flush by using stream
      sumOfFacesTextField.setText(Integer.toString(sumOfFaces));
      cardsOfHeartsTextField.setText(cardsOfHearts);

    });

    // Create a layout for the buttons
    VBox buttonsLayout = new VBox(10);
    buttonsLayout.getChildren().addAll(dealHandButton, checkHandButton);

    GridPane textFieldsLayout = new GridPane();
    textFieldsLayout.setVgap(10);
    textFieldsLayout.setHgap(10);
    textFieldsLayout.addRow(0, new TextField("Sum of Faces"), sumOfFacesTextField);
    textFieldsLayout.addRow(1, new TextField("Cards of Hearts"), cardsOfHeartsTextField);
    textFieldsLayout.addRow(2, new TextField("Flush"), flushTextField);
    textFieldsLayout.addRow(3, new TextField("Queen of Spades"), queenOfSpadesTextField);

    // Create a layout for the entire GUI
    BorderPane root = new BorderPane();
    root.setPadding(new Insets(10));
    root.setCenter(displayWindow);
    root.setRight(buttonsLayout);
    root.setBottom(textFieldsLayout);

    // Create a scene with the layout
    Scene scene = new Scene(root, 400, 300);

    // Set the scene for the stage and show the stage
    primaryStage.setScene(scene);
    primaryStage.setTitle("Card Game");
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
